package com.afry.constants;

/**
 * define all error code and messages for application.
 * 
 * @author anghan
 *
 */
public enum ErrorCodeMessages {

	TOLL_VEHICLE_NOT_NULL("TOLL_VEHICLE_NOT_NULL", "Vehicle should not be null."),
	TOLL_DATE_NOT_NULL("TOLL_DATE_NOT_NULL", "Date should not be null."),
	TOLL_ALL_DATE_NOT_SAME_DAY("TOLL_ALL_DATE_NOT_SAME_DAY", "All date should be on same day."),
	TOLL_TIME_RANGE_NOT_VALID("TOLL_TIME_RANGE_NOT_VALID", "Time range is not valid.");

	private ErrorCodeMessages(String errMessage) {
		this.errorMessage = errMessage;
	}

	private ErrorCodeMessages(String errCode, String errMessage) {
		this.errorCode = errCode;
		this.errorMessage = errMessage;
	}

	String errorCode;

	String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode + "," + errorMessage;
	}
}
