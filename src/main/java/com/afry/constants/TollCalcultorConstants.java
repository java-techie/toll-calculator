package com.afry.constants;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.afry.dto.TollTimeSlotDto;
import com.afry.model.VehicleType;

/**
 * class is used to define all constant for toll calculator
 * 
 * @author anghan
 *
 */
public class TollCalcultorConstants {

	public static final int TOTAL_TOLL_MAX = 80;

	public static final Set<VehicleType> TOLL_FREE_VEHICLE_SET = new HashSet<VehicleType>() {
		{
			add(VehicleType.MOTORBIKE);
			add(VehicleType.TRACTOR);
			add(VehicleType.EMERGENCY);
			add(VehicleType.DIPLOMAT);
			add(VehicleType.FOREIGN);
			add(VehicleType.MILITARY);

		}
	};

	public static final Set<String> TOLL_FREE_DATE_SET = new HashSet<String>() {
		{
			add(LocalDate.of(2013, 01, 1).toString());

			add(LocalDate.of(2013, 03, 28).toString());
			add(LocalDate.of(2013, 03, 29).toString());

			add(LocalDate.of(2013, 04, 1).toString());
			add(LocalDate.of(2013, 04, 30).toString());

			add(LocalDate.of(2013, 05, 1).toString());
			add(LocalDate.of(2013, 05, 8).toString());
			add(LocalDate.of(2013, 05, 9).toString());

			add(LocalDate.of(2013, 06, 5).toString());
			add(LocalDate.of(2013, 06, 6).toString());
			add(LocalDate.of(2013, 06, 21).toString());

			add(LocalDate.of(2013, 07, 1).toString());
			add(LocalDate.of(2013, 07, 2).toString());
			add(LocalDate.of(2013, 07, 3).toString());
			add(LocalDate.of(2013, 07, 4).toString());
			add(LocalDate.of(2013, 07, 5).toString());
			add(LocalDate.of(2013, 07, 6).toString());
			add(LocalDate.of(2013, 07, 7).toString());
			add(LocalDate.of(2013, 07, 8).toString());
			add(LocalDate.of(2013, 07, 9).toString());
			add(LocalDate.of(2013, 07, 10).toString());
			add(LocalDate.of(2013, 07, 11).toString());
			add(LocalDate.of(2013, 07, 12).toString());
			add(LocalDate.of(2013, 07, 13).toString());
			add(LocalDate.of(2013, 07, 14).toString());
			add(LocalDate.of(2013, 07, 15).toString());
			add(LocalDate.of(2013, 07, 16).toString());
			add(LocalDate.of(2013, 07, 17).toString());
			add(LocalDate.of(2013, 07, 18).toString());
			add(LocalDate.of(2013, 07, 19).toString());
			add(LocalDate.of(2013, 07, 20).toString());
			add(LocalDate.of(2013, 07, 21).toString());
			add(LocalDate.of(2013, 07, 22).toString());
			add(LocalDate.of(2013, 07, 23).toString());
			add(LocalDate.of(2013, 07, 24).toString());
			add(LocalDate.of(2013, 07, 25).toString());
			add(LocalDate.of(2013, 07, 26).toString());
			add(LocalDate.of(2013, 07, 27).toString());
			add(LocalDate.of(2013, 07, 28).toString());
			add(LocalDate.of(2013, 07, 29).toString());
			add(LocalDate.of(2013, 07, 30).toString());

			add(LocalDate.of(2013, 11, 1).toString());

			add(LocalDate.of(2013, 12, 24).toString());
			add(LocalDate.of(2013, 12, 25).toString());
			add(LocalDate.of(2013, 12, 26).toString());
			add(LocalDate.of(2013, 12, 31).toString());

		}
	};

	// we can take map to bifurcate toll fee by vehicle type
	// public static final Map<VehicleType, Set<TollTimeSlotDto>>
	// TOLL_FEES_BY_TIME_RANGE=new
	// ConcurrentHashMap<VehicleType,Set<TollTimeSlotDto>>(){};

	public static final Set<TollTimeSlotDto> TOLL_FEES_BY_TIME_RANGE = new HashSet<TollTimeSlotDto>() {
		{
			add(new TollTimeSlotDto(6, 0, 6, 29, 8));
			add(new TollTimeSlotDto(6, 30, 6, 59, 13));
			add(new TollTimeSlotDto(7, 0, 7, 59, 18));
			add(new TollTimeSlotDto(8, 0, 8, 29, 13));
			add(new TollTimeSlotDto(8, 30, 14, 59, 8));
			add(new TollTimeSlotDto(15, 0, 15, 29, 13));
			add(new TollTimeSlotDto(15, 0, 16, 59, 18));
			add(new TollTimeSlotDto(17, 0, 17, 59, 13));
			add(new TollTimeSlotDto(18, 0, 18, 29, 8));
		}
	};

}