package com.afry.validator;

import java.time.LocalDate;
import java.util.Date;

import com.afry.constants.ErrorCodeMessages;
import com.afry.model.Vehicle;
import com.afry.utils.DateUtils;

/**
 * class is used to validate vehicle and date
 * 
 * @author anghan
 *
 */
public class TollCalculatorValidator {
	public static ErrorCodeMessages validate(Vehicle vehicle, Date... dates) {
		if (vehicle == null) {
			return ErrorCodeMessages.TOLL_VEHICLE_NOT_NULL;
		}
		if (dates == null) {
			return ErrorCodeMessages.TOLL_DATE_NOT_NULL;
		}

		return isAllDateSameDay(dates);
	}

	private static ErrorCodeMessages isAllDateSameDay(Date... dates) {
		LocalDate todayLocalDate = DateUtils.dateToLocalDate(dates[0]);
		for (Date date : dates) {
			if (!todayLocalDate.toString().equalsIgnoreCase(DateUtils.dateToLocalDate(date).toString())) {
				return ErrorCodeMessages.TOLL_ALL_DATE_NOT_SAME_DAY;
			}
		}

		return null;
	}

}
