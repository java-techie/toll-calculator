package com.afry.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TollTimeSlotDto {
	private int startHr;
	private int startMnt;
	private int endHr;
	private int endMnt;
	private int fees;

	public TollTimeSlotDto(int startHr, int startMnt, int endHr, int endMnt, int fees) {
		this.startHr = startHr;
		this.startMnt = startMnt;
		this.endHr = endHr;
		this.endMnt = endMnt;
		this.fees = fees;
	}
}
