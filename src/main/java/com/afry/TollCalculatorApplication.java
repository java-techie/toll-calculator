package com.afry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.afry.model.Car;
import com.afry.model.Emergency;
import com.afry.service.TollFreeService;
import com.afry.service.impl.TollFreeServiceImpl;

public class TollCalculatorApplication {
	public static void main(String[] args) throws ParseException {
		TollFreeService tollFreeService = new TollFreeServiceImpl();

		System.out.println(tollFreeService.isTollFreeVehicle(new Car()));

		System.out.println(tollFreeService.isTollFreeVehicle(new Emergency()));

		String date_string = "01-01-2022 04:04:04";
		// Instantiating the SimpleDateFormat class
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		// Parsing the given String to Date object
		Date date = formatter.parse(date_string);

		System.out.println(tollFreeService.isTollFreeDate(date));

	}
}
