package com.afry.service;

import java.util.Date;

import com.afry.model.Vehicle;

public interface TimeSlotFeesService {
	/**
	 * Service is used to get fees by date
	 * 
	 * @param date
	 * @param vehicle
	 * @return
	 */
	public int getFeesByTime(Date date, Vehicle vehicle);
}
