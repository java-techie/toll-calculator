package com.afry.service;

import java.util.Date;

import com.afry.model.Vehicle;

public interface CalculateTollService {
	/**
	 * service is used to calculate toll by vehicle and date
	 * 
	 * @param vehicle
	 * @param dates
	 * @return
	 */
	public int calculate(Vehicle vehicle, Date... dates);
}
