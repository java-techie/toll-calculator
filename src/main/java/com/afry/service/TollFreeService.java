package com.afry.service;

import java.util.Date;

import com.afry.model.Vehicle;

public interface TollFreeService {

	/**
	 * Service is used to check vehicle is free for toll like Emergency/Military
	 * 
	 * @param vehicle
	 * @return
	 */
	public boolean isTollFreeVehicle(Vehicle vehicle);

	/**
	 * Service is used to check if date is tollfree like weekend/holiday
	 * 
	 * @param date
	 * @return
	 */
	public boolean isTollFreeDate(Date date);

}
