package com.afry.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.afry.constants.TollCalcultorConstants;
import com.afry.dto.TollTimeSlotDto;
import com.afry.model.Vehicle;
import com.afry.service.TimeSlotFeesService;

public class TimeSlotFeesServiceImpl implements TimeSlotFeesService {

	public int getFeesByTime(Date date, Vehicle vehicle) {

		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);

		Iterator<TollTimeSlotDto> tollTimeItr = TollCalcultorConstants.TOLL_FEES_BY_TIME_RANGE.iterator();

		while (tollTimeItr.hasNext()) {
			TollTimeSlotDto slot = tollTimeItr.next();
			if (slot.getStartHr() == hour && minute >= slot.getStartMnt() && minute <= slot.getEndMnt()) {
				return slot.getFees();
			} else if (hour >= slot.getStartHr() && hour < slot.getEndHr() && minute >= slot.getStartMnt()
					&& minute <= slot.getEndMnt()) {
				return slot.getFees();
			}
		}

		return 0;
	}

}
