package com.afry.service.impl;

import java.util.Date;

import com.afry.constants.TollCalcultorConstants;
import com.afry.model.Vehicle;
import com.afry.service.TollFreeService;
import com.afry.utils.DateUtils;

public class TollFreeServiceImpl implements TollFreeService {

	public boolean isTollFreeVehicle(Vehicle vehicle) {
		return TollCalcultorConstants.TOLL_FREE_VEHICLE_SET.contains(vehicle.getType());
	}

	public boolean isTollFreeDate(Date date) {
		if (DateUtils.isWeekend(date)
				|| TollCalcultorConstants.TOLL_FREE_DATE_SET.contains(DateUtils.dateToLocalDate(date).toString())) {
			return true;
		}
		return false;
	}

}
