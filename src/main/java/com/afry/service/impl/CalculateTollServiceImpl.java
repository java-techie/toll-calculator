package com.afry.service.impl;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.afry.constants.ErrorCodeMessages;
import com.afry.constants.TollCalcultorConstants;
import com.afry.model.Vehicle;
import com.afry.service.CalculateTollService;
import com.afry.service.TimeSlotFeesService;
import com.afry.service.TollFreeService;
import com.afry.validator.TollCalculatorValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculateTollServiceImpl implements CalculateTollService {

	TollCalculatorValidator validator = new TollCalculatorValidator();

	TollFreeService tollFreeService = new TollFreeServiceImpl();

	TimeSlotFeesService timeSlotFeesService = new TimeSlotFeesServiceImpl();

	/**
	 * Calculate the total toll fee for one day
	 *
	 * @param vehicle - the vehicle
	 * @param dates   - date and time of all passes on one day
	 * @return - the total toll fee for that day
	 */
	@Override
	public int calculate(Vehicle vehicle, Date... dates) {
		ErrorCodeMessages error = validator.validate(vehicle, dates);
		if (error != null) {
			log.info("errorCode=" + error.getErrorCode() + "	errorMsg=" + error.getErrorMessage());
			return -1;
		} else {
			if (tollFreeService.isTollFreeVehicle(vehicle) || (tollFreeService.isTollFreeDate(dates[0]))) {
				return 0;
			} else {
				return getTollFee(vehicle, dates);
			}
		}

	}

	
	private int getTollFee(Vehicle vehicle, Date... dates) {
		Date intervalStart = dates[0];
		int totalFee = 0;
		for (Date date : dates) {
			int nextFee = timeSlotFeesService.getFeesByTime(date, vehicle);
			int tempFee = timeSlotFeesService.getFeesByTime(intervalStart, vehicle);

			TimeUnit timeUnit = TimeUnit.MINUTES;
			long diffInMillies = date.getTime() - intervalStart.getTime();
			long minutes = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

			if (minutes <= 60) {
				if (totalFee > 0)
					totalFee -= tempFee;
				if (nextFee >= tempFee)
					tempFee = nextFee;
				totalFee += tempFee;
			} else {
				totalFee += nextFee;
			}
		}
		if (totalFee > TollCalcultorConstants.TOTAL_TOLL_MAX)
			totalFee = TollCalcultorConstants.TOTAL_TOLL_MAX;
		return totalFee;
	}

}
