package com.afry.utils;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Class is used for all generic date functions
 * 
 * @author anghan
 *
 */
public class DateUtils {
	/**
	 * check if date is weekend
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isWeekend(Date date) {
		LocalDate localDate = dateToLocalDate(date);
		if (localDate.getDayOfWeek() == DayOfWeek.SATURDAY || localDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
			return true;
		}
		return false;
	}

	/**
	 * covert date to localdate so that we can compare only date by excluding
	 * hour/mnt/sec
	 * 
	 * @param date
	 * @return
	 */
	public static LocalDate dateToLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
