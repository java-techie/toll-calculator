package com.afry.model;

public class Motorbike implements Vehicle {
	@Override
	public VehicleType getType() {
		return VehicleType.MOTORBIKE;
	}
}
