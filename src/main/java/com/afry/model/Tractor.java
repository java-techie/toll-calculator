package com.afry.model;

public class Tractor implements Vehicle {
	@Override
	public VehicleType getType() {
		return VehicleType.TRACTOR;
	}
}