package com.afry.model;

public class Car implements Vehicle {
	@Override
	public VehicleType getType() {
		return VehicleType.CAR;
	}
}