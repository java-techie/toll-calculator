package com.afry.model;

public class Diplomat implements Vehicle {
	@Override
	public VehicleType getType() {
		return VehicleType.DIPLOMAT;
	}
}