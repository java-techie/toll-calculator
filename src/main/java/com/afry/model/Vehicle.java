package com.afry.model;

/**
 * Vehicle master interface
 * 
 * @author anghan
 *
 */
public interface Vehicle {

	public VehicleType getType();

}
