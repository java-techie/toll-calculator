package com.afry.model;

/**
 * enum for vehicle
 * 
 * @author anghan
 *
 */
public enum VehicleType {

	CAR("Car"), DIPLOMAT("Diplomat"), EMERGENCY("Emergency"), FOREIGN("Foreign"), MILITARY("Military"),
	MOTORBIKE("Motorbike"), TRACTOR("Tractor");

	private final String type;

	VehicleType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
