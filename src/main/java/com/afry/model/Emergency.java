package com.afry.model;

public class Emergency implements Vehicle {
	@Override
	public VehicleType getType() {
		return VehicleType.EMERGENCY;
	}
}