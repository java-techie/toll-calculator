package com.afry;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.afry.model.Car;
import com.afry.model.Emergency;
import com.afry.service.CalculateTollService;
import com.afry.service.impl.CalculateTollServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculateTollTest {

	CalculateTollService tollService = new CalculateTollServiceImpl();
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Test
	public void testVehicleNullCheck() {
		assertEquals(tollService.calculate(null, new Date()), -1);
	}

	@Test
	public void testDateNullCheck() {
		assertEquals(tollService.calculate(new Car(), null), -1);
	}

	@Test
	public void testIsAllDateOnSameDay() throws ParseException {
		assertEquals(tollService.calculate(new Car(), new Date(), formatter.parse("13-06-2021 04:04:04")), -1);
	}

	@Test
	public void testIsTollFreeVehile() throws ParseException {
		assertEquals(tollService.calculate(new Emergency(), formatter.parse("13-06-2021 06:04:04")), 0); // Emergency
																											// entity is
																											// free
	}

	@Test
	public void testIsTollFreeDay() throws ParseException {
		assertEquals(tollService.calculate(new Car(), formatter.parse("01-01-2013 06:04:04")), 0); // holiday
		assertEquals(tollService.calculate(new Car(), formatter.parse("13-06-2021 06:04:04")), 0); // weekend
	}

	@Test
	public void testSingleDateFee() throws ParseException {
		assertEquals(tollService.calculate(new Car(), formatter.parse("14-06-2021 06:04:04")), 8);
	}

	@Test
	public void testMultipleDateFee1() throws ParseException {

		assertEquals(tollService.calculate(new Car(), formatter.parse("14-06-2021 06:04:04"),
				formatter.parse("14-06-2021 08:04:04"), formatter.parse("14-06-2021 18:04:04")), 29); // 8+13+8
	}

	@Test
	public void testMultipleDateFee2() throws ParseException {
		assertEquals(tollService.calculate(new Car(), formatter.parse("14-06-2021 06:04:04"),
				formatter.parse("14-06-2021 06:25:04"), formatter.parse("14-06-2021 06:30:04")), 13); // 8, 8,
																										// max(8,13)=13

	}

}
