# Toll-Calculator
This project help us to calculate toll base on vehicle and date.


## Assumption
All Date is on same day

Toll is common for all vehicle

Date coming as sorted form

Time range is 1 to 24


## Tools
JDK 8 ,
Maven

## Build
```
cd <any_folder>
git clone https://gitlab.com/java-techie/toll-calculator.git
mvn clean package
```

Note : it will execute all junit.


## Proposed next steps/improvements:
- We can take toll fees base on vehicle as toll fees will be differ base on vehicle (in code we can use concurrent hashmap with key as vehicle and value will SET with all toll fees by time slot)
- We can implement code for multiple date with different days (right now only consider toll for same days)
- We can do toll dynamic for slot (update toll fees runtime so that it will pickup latest data)
